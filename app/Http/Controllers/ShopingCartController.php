<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShopingCartController extends Controller
{
    public function index(){

        $arrDisct = array();
        
        // mengambil data dari table
        $dtDiscount = DB::table('dt_diskon')->get();
        
        foreach($dtDiscount as $item){
            $arr = array(
                'prosentage'=>$item->prosentage,
                'diskonHarga'=>$item->diskonHarga,
                'kodeBarang'=>$item->kodeBarang,
                'maxHarga'=>$item->maxHarga,
                'dateTimeDiskon'=>$item->dateTimeDiskon,
            );  

            $arrTmp[$item->kodeDiskon] = $arr;
        }

        $dtSnap = DB::table('shopcart')
        ->leftJoin('dt_barang', 'shopcart.kodeBarang', '=', 'dt_barang.kodeBarang')
        ->where('shopcart.refGroupID', '=', 'test_001')
        ->get();

        // mengirim data pegawai ke view index
        return view('index',[
            'dtShoping' => $dtSnap,
            'dtDiscount' => $arrTmp,
            
            ]);
    }
}
