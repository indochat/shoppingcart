<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shopping Cart</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script>
        var arrPrice = [];
    </script>
</head>
<body class="antialiased font-sans bg-gray-200">
    <div class="container mx-auto px-4 sm:px-8">
        <div class="py-16">
            <div>
                <h2 class="text-2xl text-center leading-tight">Troli Anda</h2>
            </div>
            <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 my-16 overflow-x-auto">
                <div class="inline-block min-w-full  overflow-hidden">
                    <table class="min-w-full leading-normal">
                        <thead>
                            <tr>
                                <th
                                    class="px-5 py-3 border-b-2 border-gray-800  text-left text-xs font-bold text-gray-800 uppercase tracking-wider">
                                    PRODUK
                                </th>
                                <th
                                    class="px-5 py-3 border-b-2 border-gray-800  text-left text-xs font-bold text-gray-800 uppercase tracking-wider">
                                    HARGA
                                </th>
                                <th
                                    class="px-5 py-3 border-b-2 border-gray-800  text-left text-xs font-bold text-gray-800 uppercase tracking-wider">
                                    KUANTITAS
                                </th>
                                <th
                                    class="px-5 py-3 border-b-2 border-gray-800  text-left text-xs font-bold text-gray-800 uppercase tracking-wider">
                                    SUBTOTAL
                                </th>
                                <th
                                    class="px-5 py-3 border-b-2 border-gray-800  text-left text-xs font-bold text-gray-800 uppercase tracking-wider">
                                    HAPUS
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($dtShoping as $key=>$item){ ?>
                            <script>
                                var price = "<?php echo $item->hargaBarang; ?>";
                                arrPrice.push(price);

                            </script>
                            <tr id="line_<?php echo $item->kodeBarang; ?>">
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <div class="flex items-center">
                                        <div class="flex-shrink-0 w-10 h-10">
                                            <img class="w-full h-full rounded-full"
                                                src="{{ asset('/images/'.$item->fotoBarang) }}"
                                                alt="" />
                                        </div>
                                        <div class="ml-3">
                                            <p class="text-gray-900 whitespace-no-wrap">
                                                <?php echo $item->namaBarang; ?>
                                            </p>
                                        </div>
                                    </div>
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <p class="text-gray-900 whitespace-no-wrap">Rp <?php echo $item->hargaBarang; ?></p>
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <div class="custom-number-input h-10 w-32">
                                        <div class="flex flex-row h-10 w-full rounded-lg relative bg-transparent mt-1">
                                            <button data-seq="<?php echo $key; ?>" data-key="<?php echo $item->kodeBarang; ?>" data-price="<?php echo $item->hargaBarang; ?>" data-action="decrement" class=" bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-l cursor-pointer outline-none">
                                            <span class="m-auto text-2xl font-thin">−</span>
                                            </button>
                                            <input type="number" class="outline-none focus:outline-none text-center w-full bg-gray-300 font-semibold text-md hover:text-black focus:text-black  md:text-basecursor-default flex items-center text-gray-700  outline-none" name="custom-input-number" value="1"/>
                                            <button data-seq="<?php echo $key; ?>" data-key="<?php echo $item->kodeBarang; ?>" data-action="increment" data-price="<?php echo $item->hargaBarang; ?>" class="bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-r cursor-pointer">
                                                <span class="m-auto text-2xl font-thin">+</span>
                                            </button>
                                    </div>
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <span
                                        class="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                                        <span aria-hidden class="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                                        <span id="dt_<?php echo $item->kodeBarang; ?>" class="relative">Rp <?php echo $item->hargaBarang; ?></span>
                                    </span>
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <span data-line="line_<?php echo $item->kodeBarang; ?>" class="removeLine cursor-pointer relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 365.696 365.696" class="w-4 h-4" xml:space="preserve"><g><path d="m243.1875 182.859375 113.132812-113.132813c12.5-12.5 12.5-32.765624 0-45.246093l-15.082031-15.082031c-12.503906-12.503907-32.769531-12.503907-45.25 0l-113.128906 113.128906-113.132813-113.152344c-12.5-12.5-32.765624-12.5-45.246093 0l-15.105469 15.082031c-12.5 12.503907-12.5 32.769531 0 45.25l113.152344 113.152344-113.128906 113.128906c-12.503907 12.503907-12.503907 32.769531 0 45.25l15.082031 15.082031c12.5 12.5 32.765625 12.5 45.246093 0l113.132813-113.132812 113.128906 113.132812c12.503907 12.5 32.769531 12.5 45.25 0l15.082031-15.082031c12.5-12.503906 12.5-32.769531 0-45.25zm0 0" data-original="#000000" class="active-path" style="fill:#C12020" data-old_color="#000000"></path></g> </svg>
                                    </span>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="2" class=" px-5 py-2 bg-white text-sm"></td>
                                <td class="modal-open px-5 py-2 cursor-pointer bg-white text-sm text-green-600 flex">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" class="w-4 h-4"><g fill="#38a169"><g><g><path d="M512,34.88c0.039-9.349-3.569-18.126-10.159-24.716S486.457-0.028,477.126,0.006c-9.322,0.039-18.098,3.702-24.711,10.314 c-2.8,2.8-7.893,8.848-10.679,12.205c-2.926,3.525-2.442,8.755,1.083,11.68c3.524,2.926,8.754,2.442,11.68-1.083 c4.364-5.256,7.971-9.395,9.646-11.071c3.498-3.497,8.132-5.435,13.05-5.456c0.027,0,0.052,0,0.079,0 c4.862,0,9.419,1.88,12.837,5.299c3.437,3.437,5.32,8.024,5.299,12.916c-0.021,4.917-1.959,9.551-5.456,13.05 c-3.131,3.131-13.893,11.668-29.312,22.439c-9.121-15.594-26.037-26.099-45.37-26.099H293.808 c-16.396,0-31.81,6.385-43.405,17.978L7.613,304.971C2.704,309.88,0,316.407,0,323.349c0,6.942,2.704,13.47,7.613,18.378 l162.667,162.667c5.068,5.067,11.722,7.6,18.378,7.6c6.656,0,13.312-2.534,18.378-7.6l242.792-242.791 c11.593-11.594,17.978-27.008,17.978-43.405V96.735c0-3.585-0.363-7.085-1.051-10.47c17.551-12.077,30.435-22.18,34.929-26.674 C508.298,52.979,511.961,44.203,512,34.88z M451.217,218.198c0,11.965-4.66,23.214-13.119,31.673L195.306,492.664 c-3.664,3.665-9.63,3.665-13.295,0L19.344,329.997c-1.775-1.775-2.754-4.136-2.754-6.648s0.978-4.872,2.754-6.647L262.135,73.911 c8.461-8.46,19.709-13.119,31.673-13.119h121.463c13.64,0,25.53,7.637,31.618,18.859c-9.798,6.488-20.769,13.387-32.408,20.171 c-0.363-0.398-0.734-0.792-1.119-1.177c-13.584-13.584-35.686-13.584-49.27,0c-13.584,13.584-13.584,35.686,0,49.27 c6.792,6.792,15.714,10.187,24.635,10.187c8.921,0,17.843-3.395,24.635-10.187c9.067-9.067,12.072-21.926,9.036-33.517 c10.123-5.893,19.844-11.916,28.815-17.743c0.001,0.028,0.003,0.054,0.003,0.081V218.198z M381.319,127.007 c1.457,2.897,4.381,4.569,7.417,4.569c1.253,0,2.526-0.285,3.722-0.887c4.862-2.446,9.707-4.99,14.504-7.596 c0.048,4.735-1.722,9.485-5.328,13.091c-7.116,7.115-18.692,7.115-25.808,0c-7.115-7.116-7.115-18.692,0-25.808 c3.558-3.558,8.231-5.336,12.904-5.336c3.7,0,7.389,1.134,10.536,3.363c-4.695,2.552-9.46,5.051-14.263,7.468 C380.908,117.928,379.259,122.915,381.319,127.007z" data-original="#000000" class="active-path" style="fill:#374957" data-old_color="#000000"></path></g></g><g><g><path d="M247.162,168.962c-4.581,0-8.295,3.713-8.295,8.295v175.179c0,4.581,3.714,8.295,8.295,8.295s8.295-3.714,8.295-8.295 V177.257C255.457,172.675,251.743,168.962,247.162,168.962z" data-original="#000000" class="active-path" style="fill:#374957" data-old_color="#000000"></path></g></g><g><g><path d="M209.231,240.213c-13.583-13.586-35.686-13.585-49.268-0.001c-13.584,13.584-13.584,35.686,0,49.27 c6.793,6.793,15.713,10.188,24.635,10.187c8.92,0,17.843-3.397,24.634-10.187c6.581-6.581,10.205-15.329,10.205-24.635 S215.812,246.792,209.231,240.213z M197.501,277.75c-7.116,7.116-18.692,7.115-25.807,0c-7.115-7.116-7.115-18.692,0-25.808 c3.558-3.558,8.231-5.336,12.904-5.336s9.346,1.778,12.904,5.336c3.447,3.447,5.345,8.029,5.345,12.904 C202.846,269.721,200.948,274.303,197.501,277.75z" data-original="#000000" class="active-path" style="fill:#374957" data-old_color="#000000"></path></g></g><g><g><path d="M334.36,240.212c-13.584-13.585-35.687-13.584-49.268,0c-13.584,13.584-13.584,35.686,0,49.27 c6.792,6.792,15.713,10.187,24.635,10.187c8.921,0,17.843-3.395,24.634-10.187C347.944,275.898,347.944,253.796,334.36,240.212z M322.629,277.75c-7.116,7.116-18.692,7.115-25.807,0c-7.115-7.116-7.115-18.692,0-25.808c3.558-3.558,8.231-5.336,12.904-5.336 s9.346,1.778,12.904,5.336C329.745,259.058,329.745,270.634,322.629,277.75z" data-original="#000000" class="active-path" style="fill:#374957" data-old_color="#000000"></path></g></g></g> </svg>
                                    <span class="mx-1"> Gunakan kode kupon</span>
                                </td>
                                <td colspan="2" class=" px-5 py-2 bg-white text-sm"></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="px-5 py-2 bg-white text-sm"></td>
                                <td class="px-5 py-2 bg-white text-sm">
                                Subtotal
                                </td>
                                <td id="subTotal" colspan="2" class="px-5 py-2 bg-white text-sm">Rp 0</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="px-5 py-2 bg-white text-sm"></td>
                                <td class="px-5 py-2 bg-white text-sm">
                                    Discount
                                </td>
                                <td id="discount" colspan="2" class="px-5 py-2 bg-white text-sm">-</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="px-5 py-2 bg-white text-sm"></td>
                                <td class="px-5 py-2 bg-white text-sm">
                                    Total
                                </td>
                                <td id="totalSemua" colspan="2" class="px-5 py-2 bg-white text-sm">Rp 0</td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>

    <!-- modal -->
    <?php 
        echo "
        <script>
            var arrDiscount = ".json_encode($dtDiscount).";
        </script>
        ";
    ?>
    <div class="modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
        <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
        
        <div class="modal-container bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
            
            <div class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
                <svg class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                    <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
                </svg>
            </div>

            <!-- Add margin if you want to see some of the overlay behind the modal-->
            <div class="modal-content py-4 text-left px-6">
            <!--Title-->
            <div class="flex justify-between items-center pb-3">
                <p class="text-2xl font-bold">Kode Discount</p>
                <div class="modal-close cursor-pointer z-50">
                <svg id="closeModal" class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                    <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
                </svg>
                </div>
            </div>

            <!--Body-->
            <div class="flex items-center border-b border-b-2 border-teal-500 py-2">
                <input id="kodeDisc" class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none" type="text" placeholder="">
                <button id="btnSetDisc" class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded" type="button">
                  Terapkan
                </button>
            </div>

            
            </div>
        </div>
    </div>
        
    <script>
        function decrement(e) {
            const btn = e.target.parentNode.parentElement.querySelector(
            'button[data-action="decrement"]'
            );
            
            var cekPrice = e.target.parentNode.dataset.price;
            var getKey = e.target.parentNode.dataset.key;
            var seqKey = e.target.parentNode.dataset.seq;
            const target = btn.nextElementSibling;
            let value = Number(target.value);

            if(value > 1){
                value--;
                target.value = value;
                var subTotalItem = Number(cekPrice) * target.value;
                arrPrice[seqKey] = subTotalItem;
                updateTotal();
                document.getElementById("dt_"+getKey).innerHTML = "Rp "+subTotalItem;
            }
        }
        
        function increment(e) {
            const btn = e.target.parentNode.parentElement.querySelector(
                'button[data-action="decrement"]'
            );

            var cekPrice = e.target.parentNode.dataset.price;
            var getKey = e.target.parentNode.dataset.key;
            var seqKey = e.target.parentNode.dataset.seq;
            const target = btn.nextElementSibling;
            let value = Number(target.value);

            if(value != 0){
                value++;
                target.value = value;
                var subTotalItem = Number(cekPrice) * target.value;
                arrPrice[seqKey] = subTotalItem;
                updateTotal();
                document.getElementById("dt_"+getKey).innerHTML = "Rp "+subTotalItem;
            }
        }
        
        const decrementButtons = document.querySelectorAll(
            `button[data-action="decrement"]`
        );
        
        const incrementButtons = document.querySelectorAll(
            `button[data-action="increment"]`
        );
        
        decrementButtons.forEach(btn => {
            btn.addEventListener("click", decrement);
        });
        
        incrementButtons.forEach(btn => {
            btn.addEventListener("click", increment);
        });

        function cekDiscount(){
            var val = $('#kodeDisc').val();
            var res = arrDiscount[val];
            if(res == undefined){
                $('#closeModal').trigger('click');
                alert('Kode tidak ditemukan!');
                return false;
            }


        }
        
        function updateTotal(){
            var totalHarga = 0;
            for(var i=0;i<arrPrice.length;i++){
                totalHarga = totalHarga + Number(arrPrice[i]);
            }

            $('#subTotal').text('Rp '+totalHarga);
            $('#totalSemua').text('Rp '+totalHarga);
        }

        //modal actions
        var openmodal = document.querySelectorAll('.modal-open')
        for (var i = 0; i < openmodal.length; i++) {
            openmodal[i].addEventListener('click', function(event){
                event.preventDefault()
                toggleModal()
            })
        }
        
        const overlay = document.querySelector('.modal-overlay')
        overlay.addEventListener('click', toggleModal)
        
        var closemodal = document.querySelectorAll('.modal-close')
        for (var i = 0; i < closemodal.length; i++) {
            closemodal[i].addEventListener('click', toggleModal)
        }
        
        document.onkeydown = function(evt) {
        evt = evt || window.event
        var isEscape = false
        if ("key" in evt) {
            isEscape = (evt.key === "Escape" || evt.key === "Esc")
        } else {
            isEscape = (evt.keyCode === 27)
        }
        if (isEscape && document.body.classList.contains('modal-active')) {
            toggleModal()
        }
        };
        
        
        function toggleModal () {
            const body = document.querySelector('body')
            const modal = document.querySelector('.modal')
            modal.classList.toggle('opacity-0')
            modal.classList.toggle('pointer-events-none')
            body.classList.toggle('modal-active')
        }
        //end modal actions

        $(document).ready(function(){
            $('.removeLine').on('click',function(){
                var line = $(this).data('line');
                $('#'+line).remove()
            })

            updateTotal();
            $('#btnSetDisc').click(function(){
                cekDiscount();
            })
        });
    </script>
</body>
</html>