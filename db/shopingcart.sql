-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Bulan Mei 2020 pada 15.06
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopingcart`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dt_barang`
--

CREATE TABLE `dt_barang` (
  `dt_barangID` int(11) NOT NULL,
  `kodeBarang` varchar(100) NOT NULL,
  `namaBarang` text NOT NULL,
  `fotoBarang` text NOT NULL,
  `stock` varchar(20) NOT NULL DEFAULT 'isAvail',
  `hargaBarang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `dt_barang`
--

INSERT INTO `dt_barang` (`dt_barangID`, `kodeBarang`, `namaBarang`, `fotoBarang`, `stock`, `hargaBarang`) VALUES
(1, 'FA4532', 'Purple Reign FA', 'img-item-1.jpg', 'isAvail', 455000),
(2, 'FA3518', 'Enchanting Belle', 'img-item-2.jpg', 'isAvail', 366000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dt_diskon`
--

CREATE TABLE `dt_diskon` (
  `dt_diskonID` int(11) NOT NULL,
  `kodeDiskon` varchar(10) DEFAULT NULL,
  `prosentage` varchar(10) DEFAULT NULL,
  `diskonHarga` int(10) DEFAULT NULL,
  `kodeBarang` varchar(10) DEFAULT NULL,
  `maxHarga` int(10) DEFAULT NULL,
  `dateTimeDiskon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `dt_diskon`
--

INSERT INTO `dt_diskon` (`dt_diskonID`, `kodeDiskon`, `prosentage`, `diskonHarga`, `kodeBarang`, `maxHarga`, `dateTimeDiskon`) VALUES
(1, 'FA111', '10', NULL, NULL, NULL, NULL),
(2, 'FA222', NULL, 50000, 'FA4532', NULL, NULL),
(3, 'FA333', '6', NULL, NULL, 400000, NULL),
(4, 'FA444', NULL, NULL, NULL, NULL, '2020-05-05 13:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `shopcart`
--

CREATE TABLE `shopcart` (
  `shopCartID` int(11) NOT NULL,
  `refGroupID` varchar(10) NOT NULL,
  `kodeBarang` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `shopcart`
--

INSERT INTO `shopcart` (`shopCartID`, `refGroupID`, `kodeBarang`) VALUES
(1, 'test_001', 'FA4532'),
(2, 'test_001', 'FA3518');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trx`
--

CREATE TABLE `trx` (
  `trxID` int(11) NOT NULL,
  `shoppingID` varchar(10) NOT NULL,
  `createDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `trx`
--

INSERT INTO `trx` (`trxID`, `shoppingID`, `createDate`) VALUES
(1, 'test_001', '2020-05-03 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dt_barang`
--
ALTER TABLE `dt_barang`
  ADD PRIMARY KEY (`dt_barangID`);

--
-- Indeks untuk tabel `dt_diskon`
--
ALTER TABLE `dt_diskon`
  ADD PRIMARY KEY (`dt_diskonID`);

--
-- Indeks untuk tabel `shopcart`
--
ALTER TABLE `shopcart`
  ADD PRIMARY KEY (`shopCartID`);

--
-- Indeks untuk tabel `trx`
--
ALTER TABLE `trx`
  ADD PRIMARY KEY (`trxID`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `dt_barang`
--
ALTER TABLE `dt_barang`
  MODIFY `dt_barangID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `dt_diskon`
--
ALTER TABLE `dt_diskon`
  MODIFY `dt_diskonID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `shopcart`
--
ALTER TABLE `shopcart`
  MODIFY `shopCartID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `trx`
--
ALTER TABLE `trx`
  MODIFY `trxID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
